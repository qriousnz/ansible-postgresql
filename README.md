PostgreSQL role for Ansible
===========================

A role for deploying and configuring [PostgreSQL](http://www.postgresql.org/) and extensions on unix hosts using [Ansible](http://www.ansibleworks.com/).

Supports
--------
- Development headers - `postgresql_dev_headers`
- Contrib modules - `postgresql_contrib`
- [PostGIS](http://postgis.net/) - `postgresql_postgis`


Usage
-----
Clone this repo into your roles directory:

    $ git clone https://github.com/zenoamaro/ansible-postgresql.git roles/postgresql

And add it to your play's roles:

    - hosts: ...
      roles:
        - ansible-postgresql
        - ...

This roles comes preloaded with almost every available default. You can override each one in your hosts/group vars, in your inventory, or in your play. See the annotated defaults in `defaults/main.yml` for help in configuration. All provided variables start with `postgresql_`.

You can also use the role as a playbook. You will be asked which hosts to provision, and you can further configure the play by using `--extra-vars`.

    $ ansible-playbook -i inventory --extra-vars='{...}' main.yml
